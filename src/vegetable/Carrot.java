/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vegetable;

/**
 *
 * @author Zalak Patel
 */
public class Carrot extends Vegetables {

    public Carrot(String color, double size) {
        super(color, size);
    }

    @Override
    public String getColor() {
      return this.color;
    }

    @Override
    public double getSize() {
        return this.size;
            }
    
    public void isRipe(){
    if("orange".equals(this.color) && this.size==1.5){
        System.out.println("Carrot is riped");
    
    }
    else{
        System.out.println("Not riped");
    }
    
    }
    
}
